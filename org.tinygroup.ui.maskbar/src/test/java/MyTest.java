import junit.framework.TestCase;
import org.tinygroup.beancontainer.BeanContainer;
import org.tinygroup.beancontainer.BeanContainerFactory;
import org.tinygroup.template.TemplateContext;
import org.tinygroup.template.TemplateEngine;
import org.tinygroup.template.TemplateException;
import org.tinygroup.template.impl.TemplateContextDefault;
import org.tinygroup.tinyrunner.Runner;


public class MyTest extends TestCase {

    private static final BeanContainer beanContainer;

    static {
        Runner.init("test-application.xml", null);
        beanContainer = BeanContainerFactory.getBeanContainer(MyTest.class.getClassLoader());
    }

    private TemplateEngine templateEngine;

    @Override
    protected void setUp() throws Exception {
        templateEngine = (TemplateEngine) beanContainer.getBean("templateEngine");
    }

    public void test() throws TemplateException {
        long start = System.currentTimeMillis();
        TemplateContext context = new TemplateContextDefault();
        context.put("TINY_CONTEXT_PATH", "/path");
        templateEngine.renderTemplate("/test.page", context, System.out);
        long end = System.currentTimeMillis();
        System.out.println("渲染耗时:" + (end - start));
    }
}
