#macro bootstrapTable(mapInfo)
<table #mapToHtml(mapInfo)>
    #bodyContent
</table>
#end
#macro btTable(tableId)#set(tableId=tableId?:fmt("t%d",rand()),tableRowId="")
<div id="${tableId}" class="hidden">#bodyContent</div>
<script>
    $(function () {
        var otherInfoData = {};
        var _bttable = $("#${tableId}-table");

        function saveBsTableColums() {
            var columns = _bttable.bootstrapTable('getColumns');
            $("#${tableId}-table>thead>tr>th").each(function () {
                var _this = $(this);
                var field = _this.data("field");
                for (var i = 0, l = columns.length; i < l; i++) {
                    if (columns[i].field == field) {
                        columns[i].width = parseInt(_this.width());
                        break;
                    }
                }
            });

            localStorage.setItem("${tableId}_bs_tb_columns", JSON.stringify(columns));
        }

        _bttable.on("load-success.bs.table", function (_bt, data) {
            var options = _bttable.bootstrapTable('getOptions');
            if (typeof(data.rows)=="undefined" || data.rows.length == 0) {
                if (data.total) {
                    options.pageNumber = options.pageNumber > 1 ? (options.pageNumber - 1) : 1;
                    _bttable.bootstrapTable('refreshOptions', options);
                    _bttable.bootstrapTable('refresh');
                }
            }
            if (window.webInfo.isSafeBrowser) {
                localStorage.setItem("${tableId}_bs_tb_options", JSON.stringify(options));
            }
        }).on("reorder-column.bs.table column-switch.bs.table resize-column.bs.table", function () {
            if (window.webInfo.isSafeBrowser) {
                saveBsTableColums()
            }
        });
        $("#${tableId}-table>thead>tr>.operate").attr("data-formatter", "${tableId}operate");
        _bttable.bootstrapTable('destroy');
        var ops={};

        if (window.webInfo.isSafeBrowser) {
            if("${TABLE_CACHE}"=="true"&&"${btcache}"!="empty") {
                var t_pos= localStorage.getItem("${tableId}_bs_tb_options")
                ops = $.parseJSON(t_pos);
            }
            var columns = localStorage.getItem("${tableId}_bs_tb_columns");
            if (columns) {
                ops = $.extend({}, ops, {"columns": $.parseJSON(localStorage.getItem("${tableId}_bs_tb_columns"))});
            }
        }
        if(!$.isEmptyObject(ops)){
            $("#${tableId}-table>thead").remove();
        }
        _bttable.bootstrapTable(ops);
        $("#${tableId}").removeClass("hidden").off("click.searchtype").on("click.searchtype", "[data-search-type]", function () {
            $(this).closest(".btn-group").find("span:first").html($(this).html());
            var thisType = $(this).attr("data-search-type");
            otherInfoData[thisType] = $(this).data("key");
            _bttable.bootstrapTable('refreshOptions', $.extend(_bttable.bootstrapTable('getOptions'), {otherParam: otherInfoData}, {pageNumber: 1}));
            _bttable.bootstrapTable('refresh');
        })
    });

    function ${tableId}operate(value, row) {
        var _tpl = $("#${tableId}-operate").html(),pattern;
        if (!_tpl) {
            return "";
        }
        _tpl = _tpl.replace(/\{id\}/ig, row.${tableRowId});
        for(var stype in row){
            if(typeof(row[stype]) != "object") {
                pattern = new RegExp("\{" + stype + "\}", "gi")
                _tpl = _tpl.replace(pattern, row[stype]);
            }
        }
        return _tpl;
    }
</script>
#end

#macro btTableContent(dataUrl,btMap)#set(
defaultMap={
    "data-side-pagination":"server",
    "data-pagination":"true",
    "data-page-size":"10",
    "data-page-list":"[10, 20, 50, 100, 200,all]",
    "data-click-to-select":"true",
    "data-show-columns":"true",
    "data-search":"true",
    "data-toolbar":"#"+tableId+"-toolbar",
    "data-show-refresh":"false",
    "data-show-toggle":"false",
    "data-show-columns":"true",
    "data-show-export":"true",
    "data-export-types":"['excel','csv','xml']",
    "data-show-pagination-switch":"false",
    "data-id-field":"id",
    "data-show-footer":"false",
    "data-search-on-enter-key":"true",
    "data-cache":"false",
    "has-opera":"true",
    "data-resizable":"true",
    "data-reorderable-columns":"true"
},btMap=defaultMap.extend(btMap,true),tableRowId=btMap.get("data-id-field"))
<table id="${tableId}-table" data-url="#link(dataUrl)" #mapToHtml(btMap)>
    <thead>
    <tr>
        #bodyContent
        #if(btMap.get("has-opera"))
            <th data-field="bt_table_opera" class="operate" data-align="center" data-click-to-select="false" data-class="text-nowrap operate">操作
            </th>
        #end
    </tr>
    </thead>
</table>
#end


#macro btTableOperate()
<div id="${tableId}-operate" class="hidden">#bodyContent</div>
#end
#macro btToolBar()
<div class="btn-group hidden-xs" id="${tableId}-toolbar" role="group">
    #bodyContent
</div>
#end


#macro btForm(formMap)#set(formMap=formMap?:{"id":fmt("f%d",rand())},formid=formMap?.id?:fmt("f%d",rand()),formMap=formMap.extend({"id":formid,"method":"get"},true))
<form style="display:none" #mapToHtml(formMap)>
    #bodyContent
</form>
<script type="text/javascript">
    !function ($) {
        'use strict';
        var isFirst = true;
        var sprintf = $.fn.bootstrapTable.utils.sprintf;
        var showAvdSearch = function (that) {
            $("#${formid}").toggle(50);
            if (isFirst) {
                $("#${formid}").Validform({
                    tiptype: 1,
                    beforeSubmit: function (curform) {
                        var data = {};
                        var _searchV = $(curform).parent().find(".search input[type=text]")
                        that.options.otherParam = $("#${formid}").serializeJson();
                        that.options.ajaxOptions.traditional = true;
                        that.options.pageNumber = 1;
                        var pageList = that.options.pageList;
                        pageList = typeof pageList === 'string' ?
                                pageList.replace('[', '').replace(']', '')
                                        .replace(/ /g, '').toLowerCase().split(',') : pageList;
                        if (that.options.pageSize == "all") {
                            that.options.pageSize = pageList[0];
                        }
                        if (_searchV.length > 0 && _searchV.val() != '') {
                            _searchV.val('');
                            that.resetSearch();
                        } else {
                            that.refresh();
                        }
                        return false;
                    }
                });
            }
            isFirst = false;
        };
        var BootstrapTable = $.fn.bootstrapTable.Constructor,
                _initToolbar = BootstrapTable.prototype.initToolbar;
        BootstrapTable.prototype.initToolbar = function () {
            _initToolbar.apply(this, Array.prototype.slice.apply(arguments));
            var that = this, html = [];
            if (this.$toolbar.find('button[name="tinySearch"]').size() == 0) {
                html.push('<button class="btn btn-default" type="button" name="tinySearch" title="高级搜索"><i class="fa fa-search"></i></button>');
                this.$toolbar.children(".columns-right:first").prepend(html.join(''));
            }
            this.$toolbar.find('button[name="tinySearch"]').off('click').on('click', function() {
            showAvdSearch(that);
            });
        };
        var _form = $("#${formid}");
        _form.find("select.select2").select2();
        var _hiddenInput = _form.find("input:hidden");
        setTimeout(function () {
            _hiddenInput.each(function () {
                $(this).attr("data-value", $(this).val())
            });
        }, 500);
        _form.off("reset.hide").on("reset.hide", function () {
            _hiddenInput.each(function () {
                var v = $(this).attr("data-value");
                if (typeof(v) == "undefined") {
                    v = '';
                }
                $(this).val(v);
            })
        });
    }(jQuery);
</script>
#end