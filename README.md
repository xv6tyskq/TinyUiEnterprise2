TINYUIEnterprise是基于TINY框架的一套前后台结合的UI框架。
它实现了UI组件化、模块化，所有资源都放在jar包里，JS合并、CSS合并，组件包依赖树支持。
比如添加一个可视编辑器只要下面的指令即可：
# @tTbditor("bodycontent")#end
在线预览地址：[http://ui2.tinygroup.org/](http://ui2.tinygroup.org/)
![][image-1]



[image-1]:	http://git.oschina.net/tinyframework/TinyUiEnterprise2/raw/master/pic.png "图片展示"
